import { DataGridPageResponse } from "@types";
import { CloseTyEnum } from "./ApiEnums";

export interface Comp {
  numberKey: number;
  optionalNumber?: number;
  stringKey?: string;
  optionalStringKey?: string;
  booleanKey: boolean;
  optionalBooleanKey?: boolean;
  anyKey: any;

  numberArrayKey: number[];
  stringArrayKey?: string[];
  booleanArrayKey: boolean[];

  mapType: Record<string, any>;

  closeTy: CloseTyEnum; // enum
}

export interface CompCombo {
  value?: string;
  text?: string;
  label?: string;
}

export interface GetCompRequest {
  filter?: string;
  useYn?: any;
  pageSize?: number;
  pageNumber?: number;
}

export interface GetCompResponse {
  ds: Comp[];
  rs: Comp;
  page: DataGridPageResponse;
}

export interface GetCompComboRequest {}

export interface GetCompComboResponse {
  ds: CompCombo[];
}

export interface PutCompRequest extends Comp {}

export interface PutCompResponse {}

export abstract class ExampleRepositoryInterface {
  abstract getComp(params: GetCompRequest): Promise<GetCompResponse>;
  abstract getCompCombo(params: GetCompComboRequest): Promise<GetCompComboResponse>;
  abstract putComp(params: PutCompRequest): Promise<PutCompResponse>;
}
